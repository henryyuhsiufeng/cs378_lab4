// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Lab4PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB4_2_API ALab4PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ALab4PlayerController();

protected:
	virtual void SetupInputComponent() override;

private:
	UFUNCTION()
	void move(float value);
	UFUNCTION()
	void strafe(float value);
	UFUNCTION()
	void interactStarted();
	UFUNCTION()
	void interactEnded();
	UFUNCTION()
	void jumpStarted();
};