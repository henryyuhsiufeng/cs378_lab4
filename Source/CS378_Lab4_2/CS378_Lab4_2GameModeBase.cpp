// Copyright Epic Games, Inc. All Rights Reserved.

#include "CS378_Lab4_2GameModeBase.h"
#include "Lab4Character.h"
#include "Lab4PlayerController.h"

ACS378_Lab4_2GameModeBase::ACS378_Lab4_2GameModeBase()
{

    PlayerControllerClass = ALab4PlayerController::StaticClass();

    // set default pawn class to our character class
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'"));

    if (GEngine)
    {
        if (pawnBPClass.Object)
        {
            GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found"));
            UClass *pawnBP = (UClass *)pawnBPClass.Object;
            DefaultPawnClass = pawnBP;
        }
        else
        {
            GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found"));
            DefaultPawnClass = ALab4Character::StaticClass();
        }
    }
}
