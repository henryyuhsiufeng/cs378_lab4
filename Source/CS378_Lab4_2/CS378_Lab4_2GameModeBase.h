// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378_Lab4_2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB4_2_API ACS378_Lab4_2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACS378_Lab4_2GameModeBase();
};
