//CS378_LAB4_2_API
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Lab4Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum : uint8
{
	IDLE UMETA(DisplayName = "Idling"),
	MOVE UMETA(DisplayName = "Moving"),
	JUMP UMETA(DisplayName = "Jumping"),
	INTERACT UMETA(DisplayName = "Interacting")
};

UCLASS()
class CS378_LAB4_2_API ALab4Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALab4Character();

	UFUNCTION(BlueprintImplementableEvent)
	void Move(float valude);

	UFUNCTION(BlueprintImplementableEvent)
	void Strafe(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void InteractionStarted();

	UFUNCTION(BlueprintImplementableEvent)
	void InteractionEnded();

	UFUNCTION(BlueprintImplementableEvent)
	void JumpStarted();

	UFUNCTION(BlueprintCallable)
	bool CanPerformAction(ECharacterActionStateEnum updatedAction);

	UFUNCTION(BlueprintCallable)
	void ApplyMovement(float scale);

	UFUNCTION(BlueprintCallable)
	void ApplyStrafe(float scale);

	UFUNCTION(BlueprintCallable)
	void BeginInteraction();

	UFUNCTION(BlueprintCallable)
	void EndInteraction();

	UFUNCTION(BlueprintCallable)
	void UpdateActionState(ECharacterActionStateEnum newAction);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ECharacterActionStateEnum CharacterActionState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float InteractionLength;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	FTimerHandle InteractionTimerHandle;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;
};