// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CS378_Lab4_2/Lab4Character.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLab4Character() {}
// Cross Module References
	CS378_LAB4_2_API UEnum* Z_Construct_UEnum_CS378_Lab4_2_ECharacterActionStateEnum();
	UPackage* Z_Construct_UPackage__Script_CS378_Lab4_2();
	CS378_LAB4_2_API UClass* Z_Construct_UClass_ALab4Character_NoRegister();
	CS378_LAB4_2_API UClass* Z_Construct_UClass_ALab4Character();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
// End Cross Module References
	static UEnum* ECharacterActionStateEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CS378_Lab4_2_ECharacterActionStateEnum, Z_Construct_UPackage__Script_CS378_Lab4_2(), TEXT("ECharacterActionStateEnum"));
		}
		return Singleton;
	}
	template<> CS378_LAB4_2_API UEnum* StaticEnum<ECharacterActionStateEnum>()
	{
		return ECharacterActionStateEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECharacterActionStateEnum(ECharacterActionStateEnum_StaticEnum, TEXT("/Script/CS378_Lab4_2"), TEXT("ECharacterActionStateEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CS378_Lab4_2_ECharacterActionStateEnum_Hash() { return 3869078940U; }
	UEnum* Z_Construct_UEnum_CS378_Lab4_2_ECharacterActionStateEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CS378_Lab4_2();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECharacterActionStateEnum"), 0, Get_Z_Construct_UEnum_CS378_Lab4_2_ECharacterActionStateEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECharacterActionStateEnum::IDLE", (int64)ECharacterActionStateEnum::IDLE },
				{ "ECharacterActionStateEnum::MOVE", (int64)ECharacterActionStateEnum::MOVE },
				{ "ECharacterActionStateEnum::JUMP", (int64)ECharacterActionStateEnum::JUMP },
				{ "ECharacterActionStateEnum::INTERACT", (int64)ECharacterActionStateEnum::INTERACT },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "IDLE.DisplayName", "Idling" },
				{ "IDLE.Name", "ECharacterActionStateEnum::IDLE" },
				{ "INTERACT.DisplayName", "Interacting" },
				{ "INTERACT.Name", "ECharacterActionStateEnum::INTERACT" },
				{ "JUMP.DisplayName", "Jumping" },
				{ "JUMP.Name", "ECharacterActionStateEnum::JUMP" },
				{ "ModuleRelativePath", "Lab4Character.h" },
				{ "MOVE.DisplayName", "Moving" },
				{ "MOVE.Name", "ECharacterActionStateEnum::MOVE" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CS378_Lab4_2,
				nullptr,
				"ECharacterActionStateEnum",
				"ECharacterActionStateEnum",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(ALab4Character::execUpdateActionState)
	{
		P_GET_ENUM(ECharacterActionStateEnum,Z_Param_newAction);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateActionState(ECharacterActionStateEnum(Z_Param_newAction));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4Character::execEndInteraction)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndInteraction();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4Character::execBeginInteraction)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BeginInteraction();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4Character::execApplyStrafe)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_scale);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ApplyStrafe(Z_Param_scale);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4Character::execApplyMovement)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_scale);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ApplyMovement(Z_Param_scale);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4Character::execCanPerformAction)
	{
		P_GET_ENUM(ECharacterActionStateEnum,Z_Param_updatedAction);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanPerformAction(ECharacterActionStateEnum(Z_Param_updatedAction));
		P_NATIVE_END;
	}
	static FName NAME_ALab4Character_InteractionEnded = FName(TEXT("InteractionEnded"));
	void ALab4Character::InteractionEnded()
	{
		ProcessEvent(FindFunctionChecked(NAME_ALab4Character_InteractionEnded),NULL);
	}
	static FName NAME_ALab4Character_InteractionStarted = FName(TEXT("InteractionStarted"));
	void ALab4Character::InteractionStarted()
	{
		ProcessEvent(FindFunctionChecked(NAME_ALab4Character_InteractionStarted),NULL);
	}
	static FName NAME_ALab4Character_JumpStarted = FName(TEXT("JumpStarted"));
	void ALab4Character::JumpStarted()
	{
		ProcessEvent(FindFunctionChecked(NAME_ALab4Character_JumpStarted),NULL);
	}
	static FName NAME_ALab4Character_Move = FName(TEXT("Move"));
	void ALab4Character::Move(float valude)
	{
		Lab4Character_eventMove_Parms Parms;
		Parms.valude=valude;
		ProcessEvent(FindFunctionChecked(NAME_ALab4Character_Move),&Parms);
	}
	static FName NAME_ALab4Character_Strafe = FName(TEXT("Strafe"));
	void ALab4Character::Strafe(float value)
	{
		Lab4Character_eventStrafe_Parms Parms;
		Parms.value=value;
		ProcessEvent(FindFunctionChecked(NAME_ALab4Character_Strafe),&Parms);
	}
	void ALab4Character::StaticRegisterNativesALab4Character()
	{
		UClass* Class = ALab4Character::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplyMovement", &ALab4Character::execApplyMovement },
			{ "ApplyStrafe", &ALab4Character::execApplyStrafe },
			{ "BeginInteraction", &ALab4Character::execBeginInteraction },
			{ "CanPerformAction", &ALab4Character::execCanPerformAction },
			{ "EndInteraction", &ALab4Character::execEndInteraction },
			{ "UpdateActionState", &ALab4Character::execUpdateActionState },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics
	{
		struct Lab4Character_eventApplyMovement_Parms
		{
			float scale;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::NewProp_scale = { "scale", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Lab4Character_eventApplyMovement_Parms, scale), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::NewProp_scale,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "ApplyMovement", nullptr, nullptr, sizeof(Lab4Character_eventApplyMovement_Parms), Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_ApplyMovement()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_ApplyMovement_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics
	{
		struct Lab4Character_eventApplyStrafe_Parms
		{
			float scale;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::NewProp_scale = { "scale", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Lab4Character_eventApplyStrafe_Parms, scale), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::NewProp_scale,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "ApplyStrafe", nullptr, nullptr, sizeof(Lab4Character_eventApplyStrafe_Parms), Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_ApplyStrafe()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_ApplyStrafe_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_BeginInteraction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_BeginInteraction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_BeginInteraction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "BeginInteraction", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_BeginInteraction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_BeginInteraction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_BeginInteraction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_BeginInteraction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics
	{
		struct Lab4Character_eventCanPerformAction_Parms
		{
			ECharacterActionStateEnum updatedAction;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_updatedAction;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_updatedAction_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Lab4Character_eventCanPerformAction_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Lab4Character_eventCanPerformAction_Parms), &Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::NewProp_updatedAction = { "updatedAction", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Lab4Character_eventCanPerformAction_Parms, updatedAction), Z_Construct_UEnum_CS378_Lab4_2_ECharacterActionStateEnum, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::NewProp_updatedAction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::NewProp_updatedAction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::NewProp_updatedAction_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "CanPerformAction", nullptr, nullptr, sizeof(Lab4Character_eventCanPerformAction_Parms), Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_CanPerformAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_CanPerformAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_EndInteraction_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_EndInteraction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_EndInteraction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "EndInteraction", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_EndInteraction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_EndInteraction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_EndInteraction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_EndInteraction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_InteractionEnded_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_InteractionEnded_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_InteractionEnded_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "InteractionEnded", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_InteractionEnded_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_InteractionEnded_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_InteractionEnded()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_InteractionEnded_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_InteractionStarted_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_InteractionStarted_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_InteractionStarted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "InteractionStarted", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_InteractionStarted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_InteractionStarted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_InteractionStarted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_InteractionStarted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_JumpStarted_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_JumpStarted_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_JumpStarted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "JumpStarted", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_JumpStarted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_JumpStarted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_JumpStarted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_JumpStarted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_Move_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_valude;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ALab4Character_Move_Statics::NewProp_valude = { "valude", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Lab4Character_eventMove_Parms, valude), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALab4Character_Move_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_Move_Statics::NewProp_valude,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_Move_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_Move_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "Move", nullptr, nullptr, sizeof(Lab4Character_eventMove_Parms), Z_Construct_UFunction_ALab4Character_Move_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_Move_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_Move_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_Move_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_Move()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_Move_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_Strafe_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ALab4Character_Strafe_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Lab4Character_eventStrafe_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALab4Character_Strafe_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_Strafe_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_Strafe_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_Strafe_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "Strafe", nullptr, nullptr, sizeof(Lab4Character_eventStrafe_Parms), Z_Construct_UFunction_ALab4Character_Strafe_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_Strafe_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_Strafe_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_Strafe_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_Strafe()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_Strafe_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics
	{
		struct Lab4Character_eventUpdateActionState_Parms
		{
			ECharacterActionStateEnum newAction;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_newAction;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_newAction_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::NewProp_newAction = { "newAction", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Lab4Character_eventUpdateActionState_Parms, newAction), Z_Construct_UEnum_CS378_Lab4_2_ECharacterActionStateEnum, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::NewProp_newAction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::NewProp_newAction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::NewProp_newAction_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4Character, nullptr, "UpdateActionState", nullptr, nullptr, sizeof(Lab4Character_eventUpdateActionState_Parms), Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4Character_UpdateActionState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4Character_UpdateActionState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ALab4Character_NoRegister()
	{
		return ALab4Character::StaticClass();
	}
	struct Z_Construct_UClass_ALab4Character_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InteractionLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InteractionLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharacterActionState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CharacterActionState;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CharacterActionState_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALab4Character_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_CS378_Lab4_2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ALab4Character_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ALab4Character_ApplyMovement, "ApplyMovement" }, // 1178482027
		{ &Z_Construct_UFunction_ALab4Character_ApplyStrafe, "ApplyStrafe" }, // 1201405498
		{ &Z_Construct_UFunction_ALab4Character_BeginInteraction, "BeginInteraction" }, // 2719243345
		{ &Z_Construct_UFunction_ALab4Character_CanPerformAction, "CanPerformAction" }, // 1205682430
		{ &Z_Construct_UFunction_ALab4Character_EndInteraction, "EndInteraction" }, // 2483454126
		{ &Z_Construct_UFunction_ALab4Character_InteractionEnded, "InteractionEnded" }, // 2136465299
		{ &Z_Construct_UFunction_ALab4Character_InteractionStarted, "InteractionStarted" }, // 583992926
		{ &Z_Construct_UFunction_ALab4Character_JumpStarted, "JumpStarted" }, // 3386036931
		{ &Z_Construct_UFunction_ALab4Character_Move, "Move" }, // 1424397990
		{ &Z_Construct_UFunction_ALab4Character_Strafe, "Strafe" }, // 2017400790
		{ &Z_Construct_UFunction_ALab4Character_UpdateActionState, "UpdateActionState" }, // 1485695658
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALab4Character_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Lab4Character.h" },
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALab4Character_Statics::NewProp_InteractionLength_MetaData[] = {
		{ "Category", "Lab4Character" },
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ALab4Character_Statics::NewProp_InteractionLength = { "InteractionLength", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALab4Character, InteractionLength), METADATA_PARAMS(Z_Construct_UClass_ALab4Character_Statics::NewProp_InteractionLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALab4Character_Statics::NewProp_InteractionLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALab4Character_Statics::NewProp_CharacterActionState_MetaData[] = {
		{ "Category", "Lab4Character" },
		{ "ModuleRelativePath", "Lab4Character.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ALab4Character_Statics::NewProp_CharacterActionState = { "CharacterActionState", nullptr, (EPropertyFlags)0x0020080000020015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALab4Character, CharacterActionState), Z_Construct_UEnum_CS378_Lab4_2_ECharacterActionStateEnum, METADATA_PARAMS(Z_Construct_UClass_ALab4Character_Statics::NewProp_CharacterActionState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALab4Character_Statics::NewProp_CharacterActionState_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ALab4Character_Statics::NewProp_CharacterActionState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ALab4Character_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALab4Character_Statics::NewProp_InteractionLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALab4Character_Statics::NewProp_CharacterActionState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALab4Character_Statics::NewProp_CharacterActionState_Underlying,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALab4Character_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALab4Character>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALab4Character_Statics::ClassParams = {
		&ALab4Character::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ALab4Character_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ALab4Character_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALab4Character_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALab4Character_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALab4Character()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALab4Character_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALab4Character, 1066142410);
	template<> CS378_LAB4_2_API UClass* StaticClass<ALab4Character>()
	{
		return ALab4Character::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALab4Character(Z_Construct_UClass_ALab4Character, &ALab4Character::StaticClass, TEXT("/Script/CS378_Lab4_2"), TEXT("ALab4Character"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALab4Character);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
