// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_LAB4_2_Lab4PlayerController_generated_h
#error "Lab4PlayerController.generated.h already included, missing '#pragma once' in Lab4PlayerController.h"
#endif
#define CS378_LAB4_2_Lab4PlayerController_generated_h

#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_SPARSE_DATA
#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execjumpStarted); \
	DECLARE_FUNCTION(execinteractEnded); \
	DECLARE_FUNCTION(execinteractStarted); \
	DECLARE_FUNCTION(execstrafe); \
	DECLARE_FUNCTION(execmove);


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execjumpStarted); \
	DECLARE_FUNCTION(execinteractEnded); \
	DECLARE_FUNCTION(execinteractStarted); \
	DECLARE_FUNCTION(execstrafe); \
	DECLARE_FUNCTION(execmove);


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALab4PlayerController(); \
	friend struct Z_Construct_UClass_ALab4PlayerController_Statics; \
public: \
	DECLARE_CLASS(ALab4PlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab4_2"), NO_API) \
	DECLARE_SERIALIZER(ALab4PlayerController)


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesALab4PlayerController(); \
	friend struct Z_Construct_UClass_ALab4PlayerController_Statics; \
public: \
	DECLARE_CLASS(ALab4PlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab4_2"), NO_API) \
	DECLARE_SERIALIZER(ALab4PlayerController)


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALab4PlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALab4PlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALab4PlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALab4PlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALab4PlayerController(ALab4PlayerController&&); \
	NO_API ALab4PlayerController(const ALab4PlayerController&); \
public:


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALab4PlayerController(ALab4PlayerController&&); \
	NO_API ALab4PlayerController(const ALab4PlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALab4PlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALab4PlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALab4PlayerController)


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_PRIVATE_PROPERTY_OFFSET
#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_12_PROLOG
#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_SPARSE_DATA \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_RPC_WRAPPERS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_INCLASS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_SPARSE_DATA \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_INCLASS_NO_PURE_DECLS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB4_2_API UClass* StaticClass<class ALab4PlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab4_2_Source_CS378_Lab4_2_Lab4PlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
