// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ECharacterActionStateEnum : uint8;
#ifdef CS378_LAB4_2_Lab4Character_generated_h
#error "Lab4Character.generated.h already included, missing '#pragma once' in Lab4Character.h"
#endif
#define CS378_LAB4_2_Lab4Character_generated_h

#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_SPARSE_DATA
#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateActionState); \
	DECLARE_FUNCTION(execEndInteraction); \
	DECLARE_FUNCTION(execBeginInteraction); \
	DECLARE_FUNCTION(execApplyStrafe); \
	DECLARE_FUNCTION(execApplyMovement); \
	DECLARE_FUNCTION(execCanPerformAction);


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateActionState); \
	DECLARE_FUNCTION(execEndInteraction); \
	DECLARE_FUNCTION(execBeginInteraction); \
	DECLARE_FUNCTION(execApplyStrafe); \
	DECLARE_FUNCTION(execApplyMovement); \
	DECLARE_FUNCTION(execCanPerformAction);


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_EVENT_PARMS \
	struct Lab4Character_eventMove_Parms \
	{ \
		float valude; \
	}; \
	struct Lab4Character_eventStrafe_Parms \
	{ \
		float value; \
	};


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_CALLBACK_WRAPPERS
#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALab4Character(); \
	friend struct Z_Construct_UClass_ALab4Character_Statics; \
public: \
	DECLARE_CLASS(ALab4Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab4_2"), NO_API) \
	DECLARE_SERIALIZER(ALab4Character)


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_INCLASS \
private: \
	static void StaticRegisterNativesALab4Character(); \
	friend struct Z_Construct_UClass_ALab4Character_Statics; \
public: \
	DECLARE_CLASS(ALab4Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab4_2"), NO_API) \
	DECLARE_SERIALIZER(ALab4Character)


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALab4Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALab4Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALab4Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALab4Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALab4Character(ALab4Character&&); \
	NO_API ALab4Character(const ALab4Character&); \
public:


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALab4Character(ALab4Character&&); \
	NO_API ALab4Character(const ALab4Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALab4Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALab4Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALab4Character)


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CharacterActionState() { return STRUCT_OFFSET(ALab4Character, CharacterActionState); } \
	FORCEINLINE static uint32 __PPO__InteractionLength() { return STRUCT_OFFSET(ALab4Character, InteractionLength); }


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_17_PROLOG \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_EVENT_PARMS


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_SPARSE_DATA \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_RPC_WRAPPERS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_CALLBACK_WRAPPERS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_INCLASS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_SPARSE_DATA \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_CALLBACK_WRAPPERS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_INCLASS_NO_PURE_DECLS \
	CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB4_2_API UClass* StaticClass<class ALab4Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab4_2_Source_CS378_Lab4_2_Lab4Character_h


#define FOREACH_ENUM_ECHARACTERACTIONSTATEENUM(op) \
	op(ECharacterActionStateEnum::IDLE) \
	op(ECharacterActionStateEnum::MOVE) \
	op(ECharacterActionStateEnum::JUMP) \
	op(ECharacterActionStateEnum::INTERACT) 

enum class ECharacterActionStateEnum : uint8;
template<> CS378_LAB4_2_API UEnum* StaticEnum<ECharacterActionStateEnum>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
