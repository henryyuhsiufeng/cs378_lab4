// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CS378_Lab4_2/Lab4PlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLab4PlayerController() {}
// Cross Module References
	CS378_LAB4_2_API UClass* Z_Construct_UClass_ALab4PlayerController_NoRegister();
	CS378_LAB4_2_API UClass* Z_Construct_UClass_ALab4PlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_CS378_Lab4_2();
// End Cross Module References
	DEFINE_FUNCTION(ALab4PlayerController::execjumpStarted)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->jumpStarted();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4PlayerController::execinteractEnded)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->interactEnded();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4PlayerController::execinteractStarted)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->interactStarted();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4PlayerController::execstrafe)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->strafe(Z_Param_value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ALab4PlayerController::execmove)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->move(Z_Param_value);
		P_NATIVE_END;
	}
	void ALab4PlayerController::StaticRegisterNativesALab4PlayerController()
	{
		UClass* Class = ALab4PlayerController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "interactEnded", &ALab4PlayerController::execinteractEnded },
			{ "interactStarted", &ALab4PlayerController::execinteractStarted },
			{ "jumpStarted", &ALab4PlayerController::execjumpStarted },
			{ "move", &ALab4PlayerController::execmove },
			{ "strafe", &ALab4PlayerController::execstrafe },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ALab4PlayerController_interactEnded_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4PlayerController_interactEnded_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4PlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4PlayerController_interactEnded_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4PlayerController, nullptr, "interactEnded", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4PlayerController_interactEnded_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4PlayerController_interactEnded_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4PlayerController_interactEnded()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4PlayerController_interactEnded_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4PlayerController_interactStarted_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4PlayerController_interactStarted_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4PlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4PlayerController_interactStarted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4PlayerController, nullptr, "interactStarted", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4PlayerController_interactStarted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4PlayerController_interactStarted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4PlayerController_interactStarted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4PlayerController_interactStarted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4PlayerController_jumpStarted_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4PlayerController_jumpStarted_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4PlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4PlayerController_jumpStarted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4PlayerController, nullptr, "jumpStarted", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4PlayerController_jumpStarted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4PlayerController_jumpStarted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4PlayerController_jumpStarted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4PlayerController_jumpStarted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4PlayerController_move_Statics
	{
		struct Lab4PlayerController_eventmove_Parms
		{
			float value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ALab4PlayerController_move_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Lab4PlayerController_eventmove_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALab4PlayerController_move_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4PlayerController_move_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4PlayerController_move_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4PlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4PlayerController_move_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4PlayerController, nullptr, "move", nullptr, nullptr, sizeof(Lab4PlayerController_eventmove_Parms), Z_Construct_UFunction_ALab4PlayerController_move_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4PlayerController_move_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4PlayerController_move_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4PlayerController_move_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4PlayerController_move()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4PlayerController_move_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ALab4PlayerController_strafe_Statics
	{
		struct Lab4PlayerController_eventstrafe_Parms
		{
			float value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Lab4PlayerController_eventstrafe_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Lab4PlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ALab4PlayerController, nullptr, "strafe", nullptr, nullptr, sizeof(Lab4PlayerController_eventstrafe_Parms), Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ALab4PlayerController_strafe()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ALab4PlayerController_strafe_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ALab4PlayerController_NoRegister()
	{
		return ALab4PlayerController::StaticClass();
	}
	struct Z_Construct_UClass_ALab4PlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALab4PlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_CS378_Lab4_2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ALab4PlayerController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ALab4PlayerController_interactEnded, "interactEnded" }, // 2779909481
		{ &Z_Construct_UFunction_ALab4PlayerController_interactStarted, "interactStarted" }, // 1623020166
		{ &Z_Construct_UFunction_ALab4PlayerController_jumpStarted, "jumpStarted" }, // 2169913080
		{ &Z_Construct_UFunction_ALab4PlayerController_move, "move" }, // 1881775883
		{ &Z_Construct_UFunction_ALab4PlayerController_strafe, "strafe" }, // 4136349597
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALab4PlayerController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Lab4PlayerController.h" },
		{ "ModuleRelativePath", "Lab4PlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALab4PlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALab4PlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALab4PlayerController_Statics::ClassParams = {
		&ALab4PlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALab4PlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALab4PlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALab4PlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALab4PlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALab4PlayerController, 2639335192);
	template<> CS378_LAB4_2_API UClass* StaticClass<ALab4PlayerController>()
	{
		return ALab4PlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALab4PlayerController(Z_Construct_UClass_ALab4PlayerController, &ALab4PlayerController::StaticClass, TEXT("/Script/CS378_Lab4_2"), TEXT("ALab4PlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALab4PlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
